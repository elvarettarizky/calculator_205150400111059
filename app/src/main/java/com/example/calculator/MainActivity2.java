package com.example.calculator;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.calculator.R;

// METODE 1. BIKIN OBJEKNYA

// public class MainActivity2 extends AppCompatActivity {

//    Button bt1, bt2;
//    TextView tv1;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main2);
//        bt1 = findViewById(R.id.bt1);
//        bt2 = findViewById(R.id.bt2);
//        tv1 = findViewById(R.id.tv1);
//        bt1.setOnClickListener(myClickListener1);
//        bt2.setOnClickListener(myClickListener2);
//    }
//
//    private View.OnClickListener myClickListener1 = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            tv1.setText("Tombol 1 ditekan");
//        }
//    }
//
//    private View.OnClickListener myClickListener2 = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            tv1.setText("Tombol 2 ditekan");
//        }
//    }


//METODE KEDUA. IMPLEMENT LISTENERNYA

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {

    Button bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, bt0, bttambah, btkali, btkurang, btbagi, bthasil, btkoma, btclear;
    TextView tv1;
    Float angka1, angka2;
    boolean penjumlahan, pengurangan, perkalian, pembagian;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        bt3 = findViewById(R.id.bt3);
        bt4 = findViewById(R.id.bt4);
        bt5 = findViewById(R.id.bt5);
        bt6 = findViewById(R.id.bt6);
        bt7 = findViewById(R.id.bt7);
        bt8 = findViewById(R.id.bt8);
        bt9 = findViewById(R.id.bt9);
        bt0 = findViewById(R.id.bt0);
        bttambah = findViewById(R.id.bttambah);
        btkali = findViewById(R.id.btkali);
        btkurang = findViewById(R.id.btkurang);
        btbagi = findViewById(R.id.btbagi);
        bthasil = findViewById(R.id.bthasil);
        btkoma = findViewById(R.id.btkoma);
        btclear = findViewById(R.id.btclear);
        tv1 = findViewById(R.id.tv1);

        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
        bt3.setOnClickListener(this);
        bt4.setOnClickListener(this);
        bt5.setOnClickListener(this);
        bt6.setOnClickListener(this);
        bt7.setOnClickListener(this);
        bt8.setOnClickListener(this);
        bt9.setOnClickListener(this);
        bt0.setOnClickListener(this);
        bttambah.setOnClickListener(this);
        btkali.setOnClickListener(this);
        btkurang.setOnClickListener(this);
        btbagi.setOnClickListener(this);
        bthasil.setOnClickListener(this);
        btkoma.setOnClickListener(this);
        btclear.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.bt1):
                tv1.setText(tv1.getText() + "1");
                break;
            case (R.id.bt2):
                tv1.setText(tv1.getText() + "2");
                break;
            case (R.id.bt3):
                tv1.setText(tv1.getText() + "3");
                break;
            case (R.id.bt4):
                tv1.setText(tv1.getText() + "4");
                break;
            case (R.id.bt5):
                tv1.setText(tv1.getText() + "5");
                break;
            case (R.id.bt6):
                tv1.setText(tv1.getText() + "6");
                break;
            case (R.id.bt7):
                tv1.setText(tv1.getText() + "7");
                break;
            case (R.id.bt8):
                tv1.setText(tv1.getText() + "8");
                break;
            case (R.id.bt9):
                tv1.setText(tv1.getText() + "9");
                break;
            case (R.id.bt0):
                tv1.setText(tv1.getText() + "0");
                break;
            case (R.id.btkoma):
                tv1.setText(tv1.getText() + ".");
                break;
            case (R.id.bttambah):
                angka1 = Float.parseFloat(String.valueOf(tv1.getText()));
                penjumlahan = true;
                tv1.setText(null);
                break;
            case (R.id.btkali):
                angka1 = Float.parseFloat(String.valueOf(tv1.getText()));
                perkalian = true;
                tv1.setText(null);
                break;
            case (R.id.btkurang):
                angka1 = Float.parseFloat(String.valueOf(tv1.getText()));
                pengurangan = true;
                tv1.setText(null);
                break;
            case (R.id.btbagi):
                angka1 = Float.parseFloat(String.valueOf(tv1.getText()));
                pembagian = true;
                tv1.setText(null);
                break;
            case (R.id.bthasil):
                angka2 = Float.parseFloat(tv1.getText() + "");
                if (penjumlahan == true) {
                    tv1.setText(angka1 + angka2 + "");
                    penjumlahan = false;
                }
                if (pengurangan == true) {
                    tv1.setText(angka1 - angka2 + "");
                    pengurangan = false;
                }
                if (perkalian == true) {
                    tv1.setText(angka1 * angka2 + "");
                    perkalian = false;
                }
                if (pembagian == true) {
                    tv1.setText(angka1 / angka2 + "");
                    pembagian = false;
                }
                break;
            case (R.id.btclear):
                tv1.setText("");
                break;
        }
    }
}
